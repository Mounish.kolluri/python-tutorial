# --------------------- Demo -----------------
# def print_msg(msg):
#     def printer():
#         print(msg)
#     return printer

# main = print_msg("Hello World")
# main()

# -------------------- Example --------------------
def make_multiplier_of(n):
    def mutiplier(x):
        return x*n
    return mutiplier

times3 = make_multiplier_of(3)
print(times3(5))

times5 = make_multiplier_of(5)
print(times5(5))

# >>> make_multiplier_of.__closure__
# >>> times3.__closure__
# (<cell at 0x7f79351bd7c0: int object at 0x955e80>,)
# >>> times3.__closure__[0].cell_contents
# 3