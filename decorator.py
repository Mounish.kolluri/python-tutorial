# --------------------- function as object -----------------
# def first(msg):
#     print(msg)

# first("Hello")

# second = first
# second("Hello")

# --------------------- functions as arguments (higher order functions)------------------------
# def inc(x):
#     return x + 1

# def dec(x):
#     return x - 1

# def operate(func, x):
#     result = func(x)
#     return result


# print(operate(inc, 5))
# print(operate(dec, 6))

# ---------------------- function can return another function (closure) -------------
# def is_called():
#     def is_returned():
#         print("Hello")

#     return is_returned

# new = is_called()
# new()

# -------------- a decorator takes in a function, adds some functionality and returns it.----------------
# from closure import make_multiplier_of

# def make_pretty(fun):
#     def inner():
#         print("inside decorator")
#         fun()
#     return inner

# # ordinary = make_pretty(ordinary)
# @make_pretty
# def ordinary():
#     print("ordinary function")

# new = ordinary()

# -------------- decorating function with parameters ------------------------------------
def smart_devide(fun):
    def inner(a,b):
        print("i am deviding ", a , "and", b)
        if b==0:
            print("oops cann't devide")
            return
            
        return fun(a,b)
    return inner

@smart_devide
def devide(a,b):
    return a/b

new = devide(4,0)
print(new)
